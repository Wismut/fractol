#ifndef FRACTOL_H
# define FRACTOL_H
# include <mlx.h>
# include <stdlib.h>
# include <math.h>
# include <unistd.h>
# include <pthread.h>
# define SIZE 1000
# define KEY_UP 5
# define KEY_DOWN 4
# define KEY_LEFT 7
# define KEY_RIGHT 6
# define KEY_W 6

typedef struct	s_list
{
    void		*mlx;
    void		*win;
    void		*img;
    void		*ptr;
    char		number;
    double		zoom;
    int 		iter;
    int 		max;
    double		scale;
    double		mouse_x;
    double		mouse_y;
    int 		offset_x;
    int 		offset_y;
    int 		bpp;
    int 		size_line;
    int 		endian;
    pthread_t	pthread[5];
}				t_list;

typedef struct	s_values
{
	void		*mlx;
	void		*win;
	int 		ixmax;
	int 		iymax;
	double		cx;
	double		cy;
	double		cxmin;
	double		cxmax;
	double		cymin;
	double		cymax;
	double		pixelwidth;
	double		pixelheight;
	double		zx;
	double		zy;
	double		zx2;
	double		zy2;
	int 		iter;
	int			itermax;
	double		escaperadius;
	double		er2;
	int 		ix;
	int 		iy;

	int 		x;
	int 		y;
	int 		i;
	int 		maxiter;
	double		cre;
	double		cim;
	double		newre;
	double		newim;
	double		oldim;
	double		oldre;
	double		zoom;
	double		movex;
	double		movey;

}				t_values;

void			create_window(t_values *list);
int				key_hook(int keycode);
int				key_exit(void);
double 			module(double d);
void			init(t_values **list);
void			create_mandelbrot(t_values *l);
void			create_mandelbrot_abs(t_values *l);
void			create_julia(t_values *l);

#endif
