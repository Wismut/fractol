#include "fractol.h"

void	create_mandelbrot(t_values *l)
{
	while (++(l->iy) < l->iymax)
	{
		l->cy = l->cymin + l->iy * l->pixelheight;
		module(l->cy) < l->pixelheight / 2 ? l->cy = 0 : 0;
		l->ix = -1;
		while (++l->ix < l->ixmax)
		{
			l->cx = l->cxmin + l->ix * l->pixelwidth;
			l->zx = 0;
			l->zy = 0;
			l->zx2 = l->zx * l->zx;
			l->zy2 = l->zy * l->zy;
			l->iter = -1;
			while (++l->iter < l->itermax && ((l->zx2 + l->zy2) < l->er2))
			{
				l->zy = 2 * l->zx * l->zy + l->cy;
				l->zx = l->zx2 - l->zy2 + l->cx;
				l->zx2 = l->zx * l->zx;
				l->zy2 = l->zy * l->zy;
			}
			if (l->iter != l->itermax)
				mlx_pixel_put(l->mlx, l->win, l->ix, l->iy, 0xFFFFFF);
		}
	}
}

void	create_mandelbrot_abs(t_values *l)
{
	while (++(l->iy) < l->iymax)
	{
		l->cy = l->cymin + l->iy * l->pixelheight;
		module(l->cy) < l->pixelheight / 2 ? l->cy = 0 : 0;
		l->ix = -1;
		while (++l->ix < l->ixmax)
		{
			l->cx = l->cxmin + l->ix * l->pixelwidth;
			l->zx = 0;
			l->zy = 0;
			l->zx2 = l->zx * l->zx;
			l->zy2 = l->zy * l->zy;
			l->iter = -1;
			while (++l->iter < l->itermax && ((l->zx2 + l->zy2) < l->er2))
			{
				l->zy = 2 * l->zx * l->zy + l->cy;
				l->zx = l->zy2 - l->zx2 + l->cx;
				l->zx2 = l->zx * l->zx;
				l->zy2 = l->zy * l->zy;
			}
			if (l->iter != l->itermax)
				mlx_pixel_put(l->mlx, l->win, l->ix, l->iy, 0xFFFFFF);
		}
	}
}

void	create_julia(t_values *l)
{
	while (l->y < SIZE && ((l->x = 0) || 1))
	{
		while (l->x < SIZE && ((l->i = 0) || 1))
		{
			l->newre = 1.5 * (l->x - SIZE / 2) / (0.5 * l->zoom * SIZE) +
				l->movex;
			l->newim = (l->y - SIZE / 2) / (0.5 * l->zoom * SIZE) + l->movey;
			while (l->i < l->maxiter)
			{
				l->oldre = l->newre;
				l->oldim = l->newim;
				l->newre = l->oldre * l->oldre - l->oldim * l->oldim + l->cre;
				l->newim = 2 * l->oldre * l->oldim + l->cim;
				if ((l->newre * l->newre + l->newim * l->newim) > 4)
					break ;
				l->i++;
			}
			mlx_pixel_put(l->mlx, l->win, l->x, l->y, (l->i % 256) *
		(l->i % 256) * (l->i % 256) + 255 * 255 + 255 * (l->i < l->maxiter));
			l->x++;
		}
		l->y++;
	}
}
