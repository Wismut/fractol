#include "fractol.h"

int		main(int argc, char **argv)
{
	t_values	*list;

	list = (t_values*)malloc(sizeof(t_values));
	create_window(list);
	init(&list);
	if (argc == 1)
	{
		write(1, "Please insert number between 1 and 3\n", 37);
		exit(0);
	}
	if (argv[1][0] != '\0')
		if (argv[1][0] == '1')
			create_mandelbrot(list);
		else if (argv[1][0] == '2')
			create_mandelbrot_abs(list);
		else if (argv[1][0] == '3')
			create_julia(list);
		else
		{
			write(1, "Please insert number between 1 and 3\n", 37);
			exit(0);
		}
//	create_mandelbrot(list);
//	create_mandelbrot_abs(list);

//	create_julia(list);


	mlx_hook(list->win, 2, 5, key_hook, list);
	mlx_hook(list->win, 17, 0, key_exit, list);
	mlx_loop(list->mlx);
	return (0);
}

void	create_window(t_values *list)
{
	list->mlx = mlx_init();
	list->win = mlx_new_window(list->mlx, SIZE, SIZE, "FRACTOL");
}

int		key_hook(int keycode)
{
	if (keycode == 53)
		exit(0);
	return (0);
}

int		key_exit(void)
{
	exit(0);
}

double	module(double d)
{
	return (d >= 0 ? d : -d);
}

void	init(t_values **list)
{
	(*list)->cxmin = -2.5;
	(*list)->ixmax = SIZE;
	(*list)->iymax = SIZE;
	(*list)->cxmax = 1.5;
	(*list)->cymin = -2.0;
	(*list)->cymax = 2.0;
	(*list)->pixelwidth = ((*list)->cxmax - (*list)->cxmin) / (*list)->ixmax;
	(*list)->pixelheight = ((*list)->cymax - (*list)->cymin) / (*list)->iymax;
	(*list)->zx = 0;
	(*list)->zy = 0;
	(*list)->iter = 0;
	(*list)->itermax = 200;
	(*list)->escaperadius = 2;
	(*list)->er2 = (*list)->escaperadius * (*list)->escaperadius;
	(*list)->iy = -1;

	(*list)->zoom = 1;
	(*list)->movex = 0;
	(*list)->movey = 0;
	(*list)->maxiter = 300;
	(*list)->cre = -0.7;
	(*list)->cim = 0.27015;
	(*list)->x = 0;
	(*list)->y = 0;
}
